/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import moment from 'moment';
import React, { Component, Fragment } from 'react';
import { Image, StyleSheet, View } from 'react-native';
import { GiftedChat } from 'react-native-gifted-chat';

const address = 'ws://beta.aplicativorepasse.com.br:8777';

export default class App extends Component {
    state = {
        messages: [],
        socket: null,
        message: ''
    };

    componentDidMount = () => {
        const socket = new WebSocket(
            address + '?token=8fb6b739-06b7-411d-8710-18bfa3bab9ed'
        );

        socket.onopen = event => {
            console.log('onopen: ', event);
        };

        socket.onmessage = event => {
            console.log('onmessage: ', event);

            let new_msgs = JSON.parse(event.data);

            let pool_ids = [];
            let messages = [];

            if (!new_msgs.typing && Array.isArray(new_msgs)) {
                messages = this.state.messages.concat(
                    new_msgs.map(item => {
                        console.log('item: ', item);
                        if (item.pool_id && item.sender_id !== 9) {
                            pool_ids = pool_ids.concat(item.pool_id);
                        }

                        return {
                            date: moment(item.created_at).format('YYYY-MM-DD'),
                            msgs: [item]
                        };
                    })
                );
            } else if (!new_msgs.typing && typeof new_msgs === 'object') {
                console.log('new_msgs: ', new_msgs);
                if (new_msgs.pool_id && new_msgs.sender_id !== 9) {
                    pool_ids = pool_ids.concat(new_msgs.pool_id);
                }

                messages = this.state.messages.concat({
                    date: moment(new_msgs.created_at).format('YYYY-MM-DD'),
                    msgs: [new_msgs]
                });
            }

            if (pool_ids.length) {
                this.receiveMessages(pool_ids);
            }

            this.setState({ messages });
        };

        socket.onclose = event => {
            console.log('onclose: ', event);
        };

        socket.onerror = event => {
            console.log('onerror: ', event);
        };

        this.setState({ socket });
    };

    receiveMessages = pool_ids => {
        console.log('pool_ids: ', pool_ids);
        this.state.socket.send(
            JSON.stringify({
                ack: true,
                pool_ids: pool_ids
            })
        );
    };

    sendMessage = () => {
        this.state.socket.send(
            JSON.stringify({
                room_id: 1139,
                sender_id: 9,
                receiver_id: 286,
                message_id: moment().valueOf(),
                car: {
                    id: 1633,
                    podio_id: 1076368290,
                    user_id: 286,
                    store_id: 94,
                    color_id: 4,
                    brand_id: 109,
                    fipe_id: 39134,
                    doors: 4,
                    eixos: '',
                    status: 'aberto',
                    year: 2019,
                    observation: 'qweqwe',
                    price: 123322.33,
                    fipe_price: 217155,
                    km: 25555,
                    car_type: 0,
                    user_ddd: 85,
                    created_at: '2019-03-12T11:02:16-03:00',
                    published_at: '2019-03-27T17:18:49-03:00',
                    boosted_until: '2019-03-27 23:18:46',
                    is_edited: 0,
                    reason: '',
                    is_favorite: false,
                    published_to: [1, 2, 3, 4, 5, 1, 2, 3, 4, 5],
                    user: {
                        id: 286,
                        podio_id: null,
                        role_id: 2,
                        user_type: 1,
                        name: 'Jotinha',
                        email: 'jotinha@gmail.com',
                        dealership: 'Jota Veículos',
                        document: '37257092389',
                        zipcode: '',
                        address: null,
                        number: null,
                        neighborhood: null,
                        city: 'Horizonte',
                        state: 'CE',
                        phone: '85999506356',
                        cell_phone: '85999506356',
                        photo: 'Avatar.png',
                        latitude: null,
                        longitude: null,
                        chat_token: '012185b1-3622-40b4-854c-2834a6bfdd18',
                        device_token: null,
                        created_at: '2019-03-11T18:06:40-03:00',
                        last_seen: '2019-04-02 09:05:23',
                        is_aprovado: 1,
                        is_testing: 0,
                        expires_at: '-0001-11-30 00:00:00',
                        type: {
                            id: 1,
                            name: 'Lojista',
                            order: 2,
                            podio_id: 3
                        }
                    },
                    visible_to: [
                        {
                            id: 1,
                            name: 'Lojista'
                        },
                        {
                            id: 2,
                            name: 'Repassador'
                        },
                        {
                            id: 3,
                            name: 'Particular'
                        },
                        {
                            id: 4,
                            name: 'Corretor de Imóveis'
                        },
                        {
                            id: 5,
                            name: 'Concessionária'
                        }
                    ],
                    type: {
                        id: 0,
                        name: 'Carro',
                        created_at: '2016-08-31 19:32:20',
                        updated_at: '2016-08-31 19:32:20'
                    },
                    color: {
                        id: 4,
                        name: 'BRANCA'
                    },
                    brand: {
                        id: 109,
                        name: 'Agrale',
                        type: 0
                    },
                    fipe: {
                        id: 39134,
                        modelo_id: 1,
                        model: 'MARRUÁ AM 200 2.8 CS TDI Diesel',
                        brand_id: 109,
                        year: '2019',
                        fuel: 'Diesel',
                        code: '060007-5',
                        price: 215808,
                        type: 0
                    },
                    car_status: {
                        id: 1,
                        name: 'EM ABERTO',
                        podio_id: 4
                    },
                    image: '5c87bbe8f12f31552399336.png',
                    photos: [
                        {
                            id: 4348,
                            filename: '5c87bbe8f12f31552399336.png',
                            photo_order: null
                        },
                        {
                            id: 4349,
                            filename: '5c87bbe973f591552399337.png',
                            photo_order: null
                        },
                        {
                            id: 4350,
                            filename: '5c87bbe9cf5da1552399337.png',
                            photo_order: null
                        },
                        {
                            id: 4351,
                            filename: '5c87bbea0c5b01552399338.png',
                            photo_order: null
                        },
                        {
                            id: 4352,
                            filename: '5c87bbea09e901552399338.png',
                            photo_order: null
                        },
                        {
                            id: 4353,
                            filename: '5c87bbea4d14e1552399338.png',
                            photo_order: null
                        },
                        {
                            id: 4354,
                            filename: '5c87bbea886251552399338.png',
                            photo_order: null
                        },
                        {
                            id: 4355,
                            filename: '5c87bbea903281552399338.png',
                            photo_order: null
                        },
                        {
                            id: 4356,
                            filename: '5c87bbead58901552399338.png',
                            photo_order: null
                        },
                        {
                            id: 4357,
                            filename: '5c87bbeb25c071552399339.png',
                            photo_order: null
                        }
                    ],
                    array_optionals: [74],
                    optionals: [
                        {
                            id: 74,
                            name: 'Para-choque na cor',
                            description: ' ',
                            type: 0
                        }
                    ],
                    chats: 2,
                    calls: 1,
                    car_negotiations: 3,
                    fipe_percent_off: '-43%',
                    is_negociando: false,
                    ad: false,
                    turbo: true
                },
                client: {
                    id: 9,
                    podio_id: 1007852704,
                    role_id: 2,
                    user_type: 1,
                    name: 'Aroldo Coelho',
                    email: 'a@a.com',
                    dealership: 'Massafera Veículos',
                    document: '43066995445',
                    zipcode: '88113620',
                    address: 'Rua Jorge Turíbio Rodrigues',
                    number: '403',
                    neighborhood: 'Areias',
                    city: 'Palhoça',
                    state: 'SC',
                    phone: '48999370977',
                    cell_phone: '48999370977',
                    photo: '5c65d7f5a96db1550178293.png',
                    latitude: '-27.5524079',
                    longitude: '-48.619848',
                    chat_token: '8fb6b739-06b7-411d-8710-18bfa3bab9ed',
                    device_token: '14bfff85-6513-441d-b3df-0403183ac51f',
                    created_at: '2016-11-11T16:27:02-02:00',
                    last_seen: '2019-04-02 09:06:26',
                    is_aprovado: 1,
                    is_testing: 0,
                    expires_at: '-0001-11-30 00:00:00',
                    type: {
                        id: 1,
                        name: 'Lojista',
                        order: 2,
                        podio_id: 3
                    }
                },
                message: this.state.message,
                image: null,
                audio: null
            })
        );
    };

    formatMessages = messages => {
        let items = [];

        messages.map((item, key) => {
            return item.msgs.map((element, key) => {
                return items.push({
                    _id: element.message_id,
                    text: element.message,
                    createdAt: element.created_at,
                    user: {
                        _id: 2,
                        name: 'React Native 1',
                        avatar:
                            'https://facebook.github.io/react/img/logo_og.png'
                    },
                    image: element.image,
                    audio: element.audio,
                    video: null
                });
            });
        });

        return items;
    };

    render() {
        let messages = this.formatMessages(this.state.messages);

        return (
            <Fragment>
                {messages.length === 0 && (
                    <View
                        style={[
                            StyleSheet.absoluteFill,
                            {
                                backgroundColor: 'white',
                                justifyContent: 'center',
                                alignItems: 'center',
                                bottom: 50
                            }
                        ]}
                    >
                        <Image
                            source={{
                                uri: 'https://i.stack.imgur.com/qLdPt.png'
                            }}
                            style={{
                                ...StyleSheet.absoluteFillObject,
                                resizeMode: 'contain'
                            }}
                        />
                    </View>
                )}
                <GiftedChat
                    keyboardShouldPersistTaps="never"
                    messages={messages}
                    onSend={() => this.sendMessage()}
                    renderCustomView={this.renderCustomView}
                    user={{
                        _id: 1,
                        name: 'React Native 2',
                        avatar:
                            'https://facebook.github.io/react/img/logo_og.png'
                    }}
                    parsePatterns={linkStyle => [
                        {
                            pattern: /#(\w+)/,
                            style: { ...linkStyle, color: 'lightgreen' },
                            onPress: props => alert(`press on ${props}`)
                        }
                    ]}
                    onInputTextChanged={event =>
                        this.setState({ message: event })
                    }
                />
            </Fragment>
        );
    }
}
